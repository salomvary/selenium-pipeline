const assert = require('assert');

describe('webdriver.io page', () => {
    for (let i = 0; i < 10; i++) {
        it(`should have the right title (${i})`, () => {
            browser.url('https://webdriver.io');
            const title = browser.getTitle();
            assert.equal(title, 'WebdriverIO · Next-gen WebDriver test framework for Node.js');
        });
    }
});