const assert = require('assert');

describe('madewithvuejs.com page', () => {
    for (let i = 0; i < 10; i++) {
        it(`should have the Vue.js 2 Book (${i})`, () => {
            browser.url('https://madewithvuejs.com/');
            const categories = browser.$('a=Categories')
            categories.waitForDisplayed();
            categories.click();
            const categoriesMenu = browser.$('.categories__menu')
            categoriesMenu.waitForDisplayed();
            const books = categoriesMenu.$('a=#Books');
            books.click();
            const vuejsBook = browser.$('a=Vue.js 2 Book')
            vuejsBook.waitForDisplayed();
        });
    }
});